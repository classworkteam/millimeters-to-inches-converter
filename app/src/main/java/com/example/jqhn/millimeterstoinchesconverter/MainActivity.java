package com.example.jqhn.millimeterstoinchesconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void exitApp(View v){
        MainActivity.this.finish();
    }


    public void convert(View v){
        EditText milliText = (EditText) findViewById(R.id.millimeters_edit_text);
        EditText inchesText = (EditText) findViewById(R.id.inches_edit_text);

        double val=Double.parseDouble(milliText.getText().toString());

        inchesText.setText(Double.toString(val/25.4));

    }
}
